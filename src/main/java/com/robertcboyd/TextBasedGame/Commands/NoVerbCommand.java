package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.UI;

/**
 * This Command should be invoked any time no verb was detected
 * in the user's sentence and when the input isn't simply a shorthand
 * for another Command. It should not be used for situations when a
 * verb is simply used incorrectly or in a way that the game doesn't
 * understand. That scenario should be handled by the appropriate
 * verb Command.
 */
public class NoVerbCommand extends Command{

	private UI ui;

	/**
	 * Informs the user that no verb was detected in his/her sentence.
	 */
	@Override
	public void invoke()
	{
		ui.println("I don't see a verb in your sentence...");
	}

	public NoVerbCommand(UI ui)
	{
		this.ui = ui;
	}
}
