package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.*;
import com.robertcboyd.TextBasedGame.Parser.Sentence;

/**
 * Simplifies Command construction. Passes in the default UI, WorldManager, and
 * Player to those Commands that desire them in order to help reduce constructor
 * bloat.
 */
public class CommandFactory {
	private Player player;
	private UI ui;
	private WorldManager wm;

	public GoCommand createGoCommand(Direction direction)
	{
		return new GoCommand(player, direction, ui, wm);
	}

	public LookCommand createLookCommand(Lookable thingToLookAt)
	{
		return new LookCommand(thingToLookAt);
	}

	public BadObjectLookCommand createBadObjectLookCommand(String object)
	{
		return new BadObjectLookCommand(ui, object);
	}

	public NoVerbCommand createNoVerbCommand()
	{
		return new NoVerbCommand(ui);
	}

	public CommandFactory(Player player, UI ui, WorldManager wm)
	{
		this.player = player;
		this.ui = ui;
		this.wm = wm;
	}
}
