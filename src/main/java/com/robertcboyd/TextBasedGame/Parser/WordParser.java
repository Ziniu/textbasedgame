package com.robertcboyd.TextBasedGame.Parser;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.robertcboyd.TextBasedGame.Commands.*;
import com.robertcboyd.TextBasedGame.Base.*;
import com.robertcboyd.TextBasedGame.Parser.Word.PartOfSpeech;

/**
 * A class to clean up, tokenize, and process user input.
 */
public class WordParser {

	private CommandFactory cf;
	private Player player;

	private Map<String, String> aliases = new HashMap<String, String>();

	/**
	 * Expands aliases in the input String.
	 *
	 * @param input the String to be expanded
	 */
	public String processAliases(String input)
	{
		switch(input){
			case "n":
			case "north":
				return "go north";
			case "e":
			case "east":
				return "go east";
			case "s":
			case "south":
				return "go south";
			case "w":
			case "west":
				return "go west";
			case "l":
			case "look":
				return "look around";
			default:
				return input;
		}
	}

	//TODO examine if the raw input ought to be stored somewhere in case of failures to parse
	/**
	 * Cleans up the raw user input and removes nonletter, nonnumber characters
	 *
	 * @param rawInput String containing the raw user input
	 *
	 * @return The revised, cleaned up input
	 */
	public String parse(String rawInput)
	{
		return rawInput.toLowerCase().trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9\\s]", "");
	}

	/**
	 * Utility method that tokenizes (splits) the user's input into
	 * individual Strings. This method also finds the part of speech for
	 * each String and stores the results of these calcuations in a
	 * ArrayList<Word>.
	 *
	 * @param cleanInput Sanitized user input without multiple spaces.
	 *
	 * @return A ArrayList<Word> containing the tokenized Strings converted to Words (data objects containing a String and a PartOfSpeech)
	 */
	public ArrayList<Word> tokenize(String cleanInput)
	{
		//split based on whitespace
		String[] stringList = cleanInput.split("\\s+");

		//declare new ArrayList with words
		ArrayList<Word> wordList = new ArrayList<Word>();
		for (String s:stringList) {
			wordList.add(new Word(s, getPartOfSpeech(stringList, s)));
		}

		return wordList;
	}

	/**
	 * Attempts to discern the PartOfSpeech for a word (using context) as best as it can. This method always gives its best guess and will throw an exception if null or empty input is given.
	 * NOTE: the above is not true at this time
	 *
	 * @param tokenizedCommand A nonnull String array containing the cleaned up, tokenized user input
	 * @param wordToCheck The String containing the word in question to determine the PartOfSpeech for
	 *
	 * @return The program's best guess at the PartOfSpeech for a String
	 */
	public PartOfSpeech getPartOfSpeech(String[] tokenizedCommand, String wordToCheck)
	{
		//TODO fix this
		return PartOfSpeech.verb;
	}

	/**
	 * This method takes the user's raw input as a String and runs the appropriate Command. It does null checks to assure that the input is valid.
	 *
	 * @param rawInput The unmodified user input read from the console
	 */
	public void processInput(String rawInput)
	{
		Sentence sentence = new Sentence(this.tokenize(this.processAliases(this.parse(rawInput))));
		//TODO ensure that the Sentence is an actual good object

		//bind sentence to a Command
		Command c = bind(sentence);

		//invoke the Command
		c.invoke();
	}

	/**
	 * Attempts to assign a meaning/bind a Command to the given Sentence
	 *
	 * @param sentence The sentence that needs binding to a Command
	 *
	 * @return The appropriate Command, or NoVerbCommand if binding fails
	 */
	private Command bind(Sentence sentence)
	{
		switch(sentence.getVerb().toString()){
			case "go":
				Direction directionToGo;
				switch(sentence.getObject().toString()){
					case "north":
						directionToGo = Direction.NORTH;
						break;
					case "east":
						directionToGo = Direction.EAST;
						break;
					case "south":
						directionToGo = Direction.SOUTH;
						break;
					case "west":
						directionToGo = Direction.WEST;
						break;
					default:
						directionToGo = null;
				}
				return cf.createGoCommand(directionToGo);
			case "look":
				//TODO should check for object matching object in sentence
				ArrayList<GameObject> everythingNearby = player.getEverythingInSight();

				String object = sentence.getObject().toString();
				
				for(GameObject visibleThing:everythingNearby){
					ArrayList<String> visibleThingIdentifiers = visibleThing.getIdentifiers();
					for(String identifier:visibleThingIdentifiers){
						if(identifier.equals(object)){
							//TODO support for muliple matching objects
							return cf.createLookCommand(visibleThing);
						}
					}
				}
				return cf.createBadObjectLookCommand(object);
			default:
				return cf.createNoVerbCommand();
		}

	}

	public WordParser(CommandFactory commandFactory, Player player)
	{
		this.cf = commandFactory;
		this.player = player;
	}
}
