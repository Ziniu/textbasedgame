package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;
import java.util.List;

import com.robertcboyd.TextBasedGame.Base.UI;

/**
 * The class representing the player. Provides many methods
 * essential to interacting with the player.
 */
public class Player extends GameObject {
	private UI ui;
	private int hp;
	private Place location;
	private ArrayList<String> identifiers;
	private ArrayList<Item> inventory;

	/**
	 * Gets the current location of the Player as a Place.
	 *
	 * @return the current location of the Player
	 */
	public Place getLocation()
	{
		return location;
	}

	/**
	 * Set the Player's location to the given Place
	 *
	 * @param place the Place to be set as the Player's location
	 */
	public void setLocation(Place place)
	{
		this.location = place;
	}

	public void printDescription()
	{
		ui.println("It's you. What did you expect to see?");
	}

	public ArrayList<GameObject> getEverythingInSight()
	{
		ArrayList<GameObject> everythingNearby = getLocation().getEverything();

		ArrayList<GameObject> everythingVisible = new ArrayList<GameObject>();
		for(GameObject thing:everythingNearby){
			if(thing.isVisibleTo(this)){
				everythingVisible.add(thing);
			}
		}
		//add the player himself
		everythingVisible.add(this);
		return everythingVisible;
	}

	public boolean isVisibleTo(Player player)
	{
		if(player.getLocation().equals(this.getLocation())){
			return true;
		} else{
			return false;
		}
	}

	public ArrayList<String> getIdentifiers()
	{
		return this.identifiers;
	}

	public Player(UI ui, int hp, ArrayList<String> identifiers, ArrayList<Item> inventory)
	{
		this.ui = ui;
		this.hp = hp;
		this.identifiers = identifiers;
		this.inventory = inventory;
	}
}
