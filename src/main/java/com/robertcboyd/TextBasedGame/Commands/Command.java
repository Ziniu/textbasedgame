package com.robertcboyd.TextBasedGame.Commands;

/**
 * Any game action bindable to user input should be in a class extending
 * this class. invoke() is called when the appropriate user input is input.
 */
public abstract class Command {

	/**
	 * This method's implementation should do all of the appropriate internal work to reflect the action's consequences.
	 */
	public abstract void invoke();
}
