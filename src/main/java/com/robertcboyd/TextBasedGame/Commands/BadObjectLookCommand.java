package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.UI;

public class BadObjectLookCommand extends Command {
	private UI ui;
	private String object;

	@Override
	public void invoke()
	{
		ui.println("There doesn't appear to be a '" + object + "' anywhere nearby.");
	}

	public BadObjectLookCommand(UI ui, String object)
	{
		this.ui = ui;
		this.object = object;
	}

}
