package com.robertcboyd.TextBasedGame.Base;

/**
 * Currently supports NORTH, EAST, SOUTH, and WEST.
 */
public enum Direction
{
	NORTH, EAST, SOUTH, WEST;

	private Direction opposite;
	private String name;

	static {
		NORTH.opposite = SOUTH;
		EAST.opposite = WEST;
		SOUTH.opposite = NORTH;
		WEST.opposite = EAST;

		NORTH.name = "north";
		EAST.name = "east";
		SOUTH.name = "south";
		WEST.name = "west";
	}

	/**
	 * Obtains the opposite of the calling Direction.
	 *
	 * @return the opposite Direction of the calling one
	 */
	public Direction getOpposite() {
		return opposite;
	}

	/**
	 * Returns the direction as a String.
	 *
	 * @return the direction as a String
	 */
	@Override
	public String toString(){
		return name;
	}
}

