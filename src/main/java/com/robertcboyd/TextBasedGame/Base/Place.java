package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.Parser.WordParser;

public class Place extends GameObject {
	private String name;
	private String description;
	private WordParser wp;
	private UI ui;
	private boolean visible;
	private ArrayList<GameObject> thingsHere;
	private ArrayList<String> identifers;

	private static WorldManager worldManager = new WorldManager();

	/**
	 * Gets the WorldManager
	 *
	 * @return the WorldManager
	 */
	public static WorldManager getWorldManager()
	{
		return worldManager;
	}

	/**
	 * Called when a Player enters the Place.
	 * Prints out the Place's description and loops
	 * while the Player remains in the Place.
	 *
	 * @param player entering Player
	 */
	public void onEnter(Player player)
	{
		this.printDescription();
		do {
			wp.processInput(ui.getInput());
		} while (player.getLocation().equals(this));
	}

	/**
	 * Prints the description of this Place to the console.
	 */
	public void printDescription()
	{
		if(!this.getName().equals("")) {
			ui.println("|" + this.getName() + "|");
		}
		ui.println(this.getDescription());
	}

	/**
	 * Gets the Place's description as a String.
	 *
	 * @return a String containing the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Gets the Place's name as a String.
	 *
	 * @return a String containing the name
	 */
	public String getName()
	{
		return name;
	}

	//TODO update docs
	/**
	 * Standard Place constructor.
	 *
	 * @param name Desired name for the new Place
	 * @param description Desired description for the new Place
	 * @param wp the WordParser to use for processing
	 * @param ui the ui to use for text operations
	 *
	 * @return The newly constructed Place
	 */
	public Place(String name, String description, WordParser wp, UI ui, ArrayList<String> identifers, ArrayList<GameObject> thingsHere)
	{
		this.name = name;
		this.description = description;
		this.wp = wp;
		this.ui = ui;
		this.visible = true;
		this.identifers = identifers;
		this.thingsHere = thingsHere;
		this.thingsHere.add(this);
	}

	/**
	 * Gets the Place to the north of this Place.
	 *
	 * @return Place to the north if one exists, otherwise null.
	 */
	public Place getPlaceNorthOf()
	{
		return worldManager.getPlaceNorthOf(this);
	}

	/**
	 * Gets the Place to the east of this Place.
	 *
	 * @return Place to the east if one exists, otherwise null.
	 */
	public Place getPlaceEastOf()
	{
		return worldManager.getPlaceEastOf(this);
	}

	/**
	 * Gets the Place to the south of this Place.
	 *
	 * @return Place to the south if one exists, otherwise null.
	 */
	public Place getPlaceSouthOf()
	{
		return worldManager.getPlaceSouthOf(this);
	}

	/**
	 * Gets the Place to the west of this Place.
	 *
	 * @return Place to the west if one exists, otherwise null.
	 */
	public Place getPlaceWestOf()
	{
		return worldManager.getPlaceWestOf(this);
	}

	/**
	 * Attaches a Place to the north of this one.
	 *
	 * @param place the Place to be attached to the north
	 */
	public void attachToNorthOf(Place place)
	{
		worldManager.attachToNorthOf(this, place);
	}

	/**
	 * Attaches a Place to the east of this one.
	 *
	 * @param place the Place to be attached to the east
	 */
	public void attachToEastOf(Place place)
	{
		worldManager.attachToEastOf(this, place);
	}

	/**
	 * Attaches a Place to the south of this one.
	 *
	 * @param place the Place to be attached to the south
	 */
	public void attachToSouthOf(Place place)
	{
		worldManager.attachToSouthOf(this, place);
	}

	/**
	 * Attaches a Place to the west of this one.
	 *
	 * @param place the Place to be attached to the west
	 */
	public void attachToWestOf(Place place)
	{
		worldManager.attachToWestOf(this, place);
	}

	//TODO rewrite in case of many people
	public boolean isVisibleTo(Player player)
	{
		return visible;
	}

	public ArrayList<GameObject> getEverything()
	{
		return this.thingsHere;
	}

	public ArrayList<String> getIdentifiers()
	{
		return identifers;
	}
}
