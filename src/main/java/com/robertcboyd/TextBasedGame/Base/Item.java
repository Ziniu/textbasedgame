package com.robertcboyd.TextBasedGame.Base;

public abstract class Item extends GameObject {
	private String name;
	private String description;

	public Item(String name, String description)
	{
		this.name = name;
		this.description = description;
	}

	/**
	 * Returns the name of the Item as a String.
	 *
	 * @return a String containing the Item's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the Item's name to the desired value.
	 *
	 * @param name the name to be set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the description of the Item as a String.
	 *
	 * @return a String containing the Item's description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the Item's description to the desired value.
	 *
	 * @param description the description to be set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
