package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.Lookable;

/**
 * This Command should be invoked if the user is
 * trying to look at their surroundings
 */
public class LookCommand extends Command {

	private Lookable thingToLookAt;

	/**
	 * TODO update
	 * Causes the description for the immediate area
	 * to be printed to the console.
	 */
	@Override
	public void invoke()
	{
			thingToLookAt.printDescription();
	}

	public LookCommand(Lookable thingToLookAt)
	{
		this.thingToLookAt = thingToLookAt;
	}
}
