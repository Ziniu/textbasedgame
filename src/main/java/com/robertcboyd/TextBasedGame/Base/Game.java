package com.robertcboyd.TextBasedGame.Base;

import com.robertcboyd.TextBasedGame.Parser.WordParser;
import com.robertcboyd.TextBasedGame.Commands.CommandFactory;

/**
 * The Game class sets up the game and starts it, but
 * should not be used to hold anything more than basic
 * initialization. It is preferred that this class be
 * left relatively clean.
 */
public class Game {

	/**
	 * The main method.
	 *
	 * @param args Not used.
	 */
	public static void main(String[] args)
	{
		//game start
		UI ui = new UI(new java.util.Scanner(System.in));
		PlayerFactory playerFactory = new PlayerFactory(ui);
		Player player = playerFactory.createPlayer();
		WordParser wp = new WordParser(new CommandFactory(player, ui, Place.getWorldManager()), player);

		PlaceFactory placeFactory = new PlaceFactory(wp, ui);
		Places places = new Places(wp, ui, placeFactory);

		player.setLocation(places.firstLocation);
		while(!gameOver()) {
			player.getLocation().onEnter(player);
		}
	}

	/**
	 * Decides if the game is over.
	 *
	 * @return True if the game is over, false otherwise.
	 */
	public static boolean gameOver() {
		return false;
		//TODO for now
	}
}
