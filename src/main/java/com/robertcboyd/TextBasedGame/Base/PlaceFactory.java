package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;
import java.util.Arrays;

import com.robertcboyd.TextBasedGame.Parser.WordParser;

public class PlaceFactory {
	private WordParser wp;
	private UI ui;

	public Place createPlace(String description, GameObject... thingsHere)
	{
		ArrayList<String> identifiers = new ArrayList<String>();
		identifiers.add("here");
		identifiers.add("around");
		return new Place("", description, wp, ui, identifiers, new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}
	
	public Place createPlace(String name, String description, GameObject... thingsHere)
	{
		ArrayList<String> identifiers = new ArrayList<String>();
		identifiers.add("here");
		identifiers.add("around");
		return new Place(name, description, wp, ui, identifiers, new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}

	public Place createPlace(String name, String description, ArrayList<String> additionalIdentifiers, GameObject... thingsHere)
	{
		ArrayList<String> identifiers = new ArrayList<String>();
		identifiers.add("here");
		identifiers.add("around");

		for(String s:additionalIdentifiers){
			identifiers.add(s);
		}

		return new Place(name, description, wp, ui, identifiers, new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}

	public PlaceFactory(WordParser wp, UI ui)
	{
		this.wp = wp;
		this.ui = ui;
	}
}
